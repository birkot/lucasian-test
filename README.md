#Test JLVL REST API

Test de implementación de Spring Boot con Spring Data JPA y base de datos PostgreSQL

## Requisitos previos
Requisitos mínimos:
1. Java 12
2. PostgreSQL 12

Se debe tener configurado PostgreSQL y crear la base de datos ``lucasian_test``, Puede crear la base de datos de la siguiente manera:
    ``CREATE DATABASE lucasian_test;``


## Dependencias
El sistema esta desarrollado con [Spring Boot](http://www.spring.io/) y [Maven](https://maven.apache.org), sus librerías y dependencias seran descargadas al momento de iniciar la aplicación por primeva vez

- [Maven 4](https://maven.apache.org)
- [Spring Boot](http://www.spring.io) (2.2.8.RELEASE)
- Spring Data JPA
- Springdoc Openapi UI (1.4.1)

## Instalación y Configuración

1. Copiar el codigo fuente desde el repositorio
   ```
   git clone https://birkot@bitbucket.org/birkot/lucasian-test.git
    ```
   este repositorio contiene la carpeta del banckend ```test_jlvl``` y el frontend ``test-jlvr-frontend`` para continuar ejecutando el backend posicionate en la carpeta ``cd test_jlvl``
2. Edita el archivo de configuración
    ```
    nano src/main/resources/application.properties
    ```
    el archivo `application.properties` contiene la configuración la configuración de acceso a la base de datos y el puerto de ejecución:
   
        #Parametros de Conexión
        -------------------------
        spring.datasource.url=jdbc:postgresql://localhost:5432/lucasian_test
        spring.datasource.username=USERNAME
        spring.datasource.password=******
        spring.datasource.driverClassName=org.postgresql.Driver
        
        ##PUERTO
        server.port=8080
    
   
3. Ejecuta el sistema.
    Maven descargará las librerias nesesarias e iniciará la aplicación con los paremetros establecidos en la configuración anterior
    
    `./mvnw spring-boot:run`
    
    finalizado el proceso de inicio el sietema mostrá un msj similar al siguiente:
    
    ``2020-08-08 09:40:58.015  INFO 29261 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
      2020-08-08 09:40:58.023  INFO 29261 --- [           main] c.l.test_jlvl.TestJlvlApplication        : Started TestJlvlApplication in 14.984 seconds (JVM running for 15.938)``
   
   donde indica el puerto en el cual esta corriendo la aplicación, por defecto ```8080```, verifique que efectivamente el sistema se esta ejecutando en dicho puerto abriendo en el navegador 
   ```http://localhost:8080/swagger-ui/index.html?url=/v3/api-docs``` y se mostrará la documentación completa de la API
4. Ejecuta el sistema usando un JAR
   O puede crear el archivo JAR con
   ```./mvnw clean package ```
   Entonces puedes ejecutar el archivo JAR:
    ```java -jar target/test_jlvl-0.0.1-SNAPSHOT.jar ```
   
## Configuración y Ejecución de Frontend
Este proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 10.0.5.

1. Requisitos previos
Para instalar la CLI angular, abra una ventana de terminal y ejecute el siguiente comando:
```npm install -g @angular/cli```

2. Configuración
una vez copiado el repositorio de acuerdo a los pasos indicados en el backend dirigete al directorio 
```cd test-jlvl-frontend```
verifica el archivo ```datasourse.service.ts``` que la variable ``apiPath`` este apuntando a la url y puerto corecto del backend
```nano src/app/datasource.service.ts```

3. Instala las dependencias.
    ```npm install```

4. Ejecuta el sistema.
    ```npm start```

por defecto el sistema se ejecutará en ```http://localhost:4200/```