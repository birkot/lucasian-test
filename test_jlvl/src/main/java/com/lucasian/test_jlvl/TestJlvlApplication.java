package com.lucasian.test_jlvl;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@OpenAPIDefinition
@SpringBootApplication
public class TestJlvlApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestJlvlApplication.class, args);
	}

	/*@Bean
	public OpenAPI customOpenAPI() {

		return new OpenAPI()

				.info(new Info()
						.title("API Test Lucasian - Mercados Chapinero")
						.version("v1")
						.description("Test")
						.termsOfService("http://swagger.io/terms/")
						.license(new License().name("Apache 2.0").url("http://springdoc.org")));

	}*/
}
