package com.lucasian.test_jlvl.controllers;

import com.lucasian.test_jlvl.entity.Articles;
import com.lucasian.test_jlvl.entity.Client;
import com.lucasian.test_jlvl.entity.Partners;
import com.lucasian.test_jlvl.repository.ArticlesRepository;
import com.lucasian.test_jlvl.repository.PartnerRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "Articles Controller")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class ArticlesController {

    @Autowired
    ArticlesRepository articlesRepository;

    @Autowired
    PartnerRepository partnerRepository;

    @Operation(description = "Retorna el listado de artículos")
    @GetMapping("/api/articles")
    public List<Articles> getData() {
        return articlesRepository.findAll();
    }

    @Operation(description = "Agrega un artículo nuevo pasando todos los campos obligatorios")
    @PostMapping(value = "/api/articles", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Articles create(@Valid @RequestBody Articles articles) {
        Partners p = partnerRepository.findByPartnerId(articles.getPartner()== null ? 0 : articles.getPartner().getId());
        if(p==null){
            Partners p_new = new Partners(articles.getPartner().getVat(), articles.getPartner().getNameCompany(), articles.getPartner().getAddress());
            p = partnerRepository.save(p_new);
        }
        Articles articles_new = new Articles(articles.getCode(), articles.getName(), p);
        return articlesRepository.save(articles_new);
    }

    @Operation(description = "Edita un artículo pasando el id del articulo y todos sus parametros")
    @PutMapping(value =  "/api/articles")
    public Articles update(@Valid @RequestBody Articles articles){
        //Articles articles_update = articlesRepository.getOne(articles.getId());
        Partners p = partnerRepository.findByPartnerId(articles.getPartner()== null ? 0 : articles.getPartner().getId());
        articles.setPartner(p);
        return articlesRepository.save(articles);
    }

    @Operation(description = "Elimina un artículo pasando solo el id")
    @DeleteMapping(value =  "/api/articles")
    public ResponseEntity<Long> delete(@Valid @RequestParam Long id){
        Articles articles_delete = articlesRepository.getOne(id);
        articlesRepository.delete(articles_delete);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @Operation(description = "Busca un artículo por código")
    @GetMapping("/api/articles/search")
    public Articles search(@RequestParam String code) {
        return articlesRepository.getByCode(code);
    }

}
