package com.lucasian.test_jlvl.controllers;

import com.lucasian.test_jlvl.entity.Articles;
import com.lucasian.test_jlvl.entity.Client;
import com.lucasian.test_jlvl.entity.Invoice;
import com.lucasian.test_jlvl.entity.Partners;
import com.lucasian.test_jlvl.repository.ArticlesRepository;
import com.lucasian.test_jlvl.repository.InvoicesRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Tag(name = "Invoices Controller")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class InvoicesController {

    @Autowired
    InvoicesRepository invoicesRepository;

    @Operation(description = "Retorna el listado de facturas")
    @GetMapping("/api/invoices")
    public List<Invoice> getData() {
        return invoicesRepository.findAll();
    }

    @Operation(description = "Agrega un artículo nuevo pasando todos los campos obligatorios")
    @PostMapping(value = "/api/invoices", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Invoice create(@Valid @RequestBody Invoice invoice) {
        System.out.println(invoice.toString());
        return invoicesRepository.save(invoice);
    }

    @Operation(description = "Elimina na factura pasando el id")
    @DeleteMapping(value =  "/api/invoices")
    public ResponseEntity<Long> delete(@RequestParam Long id){
        Invoice invoice_delete = invoicesRepository.getOne(id);
        invoicesRepository.delete(invoice_delete);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

}
