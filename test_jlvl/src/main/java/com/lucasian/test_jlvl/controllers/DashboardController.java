package com.lucasian.test_jlvl.controllers;

import com.lucasian.test_jlvl.entity.ArticleByMonth;
import com.lucasian.test_jlvl.entity.Invoice;
import com.lucasian.test_jlvl.entity.SalesByClient;
import com.lucasian.test_jlvl.repository.InvoicesRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Tag(name = "Dashboard Controller")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class DashboardController {

    @Autowired
    InvoicesRepository invoicesRepository;

    @Operation(description = "Artículos mas vendidos por mes")
    @GetMapping("/api/articles/sales/bymonth")
    public List<ArticleByMonth> getArticlesSalesByMonth() {
        return invoicesRepository.findArticlesSalesByMonth();
    }

    @Operation(description = "Clientes con ventas mayores a - pasar parámetro ")
    @GetMapping("/api/clients/sales")
    public List<SalesByClient> getSalesByClient(Double min) {
        return invoicesRepository.findSalesByClient(min);
    }

    @Operation(description = "Artículos no vendidos")
    @GetMapping("/api/articles/notsales")
    public List<ArticleByMonth> getArticlesNotSales() {
        return invoicesRepository.findArticlesNotSales();
    }

}


