package com.lucasian.test_jlvl.controllers;

import com.lucasian.test_jlvl.entity.Client;
import com.lucasian.test_jlvl.entity.Partners;
import com.lucasian.test_jlvl.repository.PartnerRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Tag(name = "Partner Controller")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class PartnerController {
    @Autowired
    PartnerRepository partnerRepository;

    @Operation(description = "Retorna el listado de proveedores")
    @GetMapping("/api/partners")
    public List<Partners> getData() {
        return partnerRepository.findAll();
    }

    @Operation(description = "Agrega un cliente nuevo pasando todos los campos obligatorios")
    @PostMapping(value = "/api/partners", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Partners create(@Valid @RequestBody Partners partner) {
        return partnerRepository.save(partner);
    }

    @Operation(description = "Edita un proveedor pasando el id del proveedor y todos sus parametros")
    @PutMapping(value =  "/api/partners")
    public Partners update(@Valid @RequestBody Partners partner){
        Partners partner_update = partnerRepository.getOne(partner.getId());

        partner_update.setVat(partner.getVat());
        partner_update.setNameCompany(partner.getNameCompany());

        partner_update.setAddress(partner.getAddress());

        return partnerRepository.save(partner_update);
    }

    @Operation(description = "Elimina un proveedor pasando solo el id")
    @DeleteMapping(value =  "/api/partners")
    public ResponseEntity<Long> delete(@Valid @RequestParam Long id){
        Partners partner_delete = partnerRepository.getOne(id);
        partnerRepository.delete(partner_delete);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @Operation(description = "Busca un proveedor por documento")
    @GetMapping("/api/partners/search")
    public Partners search(@RequestParam String vat) {
        return partnerRepository.getByDocument(vat);
    }

}
