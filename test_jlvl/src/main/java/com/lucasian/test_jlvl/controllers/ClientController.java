package com.lucasian.test_jlvl.controllers;

import com.lucasian.test_jlvl.entity.Articles;
import com.lucasian.test_jlvl.entity.Client;
import com.lucasian.test_jlvl.repository.ClientRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

@RestController
@Tag(name = "Client Controller")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class ClientController {

    @Autowired
    ClientRepository clientRepository;

    @Operation(description = "Retorna el listado de clientes")
    @GetMapping("/api/clients")
    public List<Client> getData() {
        return clientRepository.findAll();
    }

    @Operation(description = "Agrega un cliente nuevo pasando todos los campos obligatorios")
    @PostMapping(value = "/api/clients", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Client create(@Valid @RequestBody Client client) {
        return clientRepository.save(client);
    }

    @Operation(description = "Edita un cliente pasando el id del cliente y todos sus parametros")
    @PutMapping(value =  "/api/clients", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Client update(@Valid @RequestBody Client client){
        Client client_update = clientRepository.getOne(client.getId());

        client_update.setDocument(client.getDocument());
        client_update.setFirstName(client.getFirstName());
        client_update.setLastName(client.getLastName());
        client_update.setAddress(client.getAddress());

        return clientRepository.save(client_update);
    }

    @Operation(description = "Elimina un cliente pasando solo el id")
    @DeleteMapping(value =  "/api/clients")
    public ResponseEntity<Long> delete(@RequestParam Long id){
        Client client_delete = clientRepository.getOne(id);
        clientRepository.delete(client_delete);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @Operation(description = "Busca un cliente por documento")
    @GetMapping("/api/clients/search")
    public Client search(@RequestParam String document) {
        return clientRepository.getByDocument(document);
    }

}
