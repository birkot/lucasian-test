package com.lucasian.test_jlvl.repository;

import com.lucasian.test_jlvl.entity.Client;
import com.lucasian.test_jlvl.entity.Partners;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public interface ClientRepository  extends JpaRepository<Client, Serializable> {

    @Query("FROM Client WHERE document = ?1")
    public Client getByDocument(String document);


}
