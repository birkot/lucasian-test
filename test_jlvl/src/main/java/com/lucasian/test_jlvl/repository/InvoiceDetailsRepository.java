package com.lucasian.test_jlvl.repository;

import com.lucasian.test_jlvl.entity.InvoiceDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

public interface InvoiceDetailsRepository   extends JpaRepository<InvoiceDetails, Serializable> {

}
