package com.lucasian.test_jlvl.repository;

import com.lucasian.test_jlvl.entity.Client;
import com.lucasian.test_jlvl.entity.Partners;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.io.Serializable;

public interface PartnerRepository  extends JpaRepository<Partners, Serializable> {

    @Query("FROM Partners WHERE id = ?1")
    public Partners findByPartnerId(Long partnerId);

    @Query("FROM Partners WHERE vat = ?1")
    public Partners getByDocument(String vat);


}
