package com.lucasian.test_jlvl.repository;

import com.lucasian.test_jlvl.entity.ArticleByMonth;
import com.lucasian.test_jlvl.entity.Invoice;
import com.lucasian.test_jlvl.entity.SalesByClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.io.Serializable;
import java.util.List;

public interface InvoicesRepository   extends JpaRepository<Invoice, Serializable> {


    @Query(value = "SELECT  TRIM(TO_CHAR(i.date_invoice, 'Month')) AS month_sale " +
            "   , art.code " +
            "   , art.name " +
            "   , SUM(ids.cant) AS cant " +
            "   , SUM(ids.subt) AS total " +
            "FROM invoice AS i " +
            "LEFT JOIN invoice_detailspk AS idpk ON idpk.invoive_id = i.id " +
            "LEFT JOIN invoice_details AS ids ON ids.id = idpk.details_id " +
            "LEFT JOIN articles AS art ON art.id = ids.article_id " +
            "GROUP BY month_sale, art.code, art.name " +
            "ORDER BY month_sale, cant DESC", nativeQuery = true)
    public List<ArticleByMonth> findArticlesSalesByMonth();

    @Query( value= "SELECT  TRIM(TO_CHAR(i.date_invoice, 'Month')) AS month_sale " +
            "               , art.code " +
            "               , art.name " +
            "               , SUM(ids.cant) AS cant " +
            "               , SUM(ids.subt) AS total " +
            "            FROM invoice AS i " +
            "            LEFT JOIN invoice_detailspk AS idpk ON idpk.invoive_id = i.id " +
            "            LEFT JOIN invoice_details AS ids ON ids.id = idpk.details_id " +
            "            RIGHT JOIN articles AS art ON art.id = ids.article_id " +
            "            WHERE cant IS NULL  " +
            "            GROUP BY month_sale, art.code, art.name", nativeQuery = true)
    public List<ArticleByMonth> findArticlesNotSales();

    @Query(value = "SELECT cl.document " +
            "   , (cl.first_name || ' ' || cl.last_name) as name " +
            "   , cl.address " +
            "   , sum(iv.total) AS total " +
            "FROM invoice AS iv " +
            "LEFT JOIN client AS cl ON iv.client_id = cl.id " +
            "WHERE total > ?1 "+
            "GROUP BY cl.document, name, cl.address ", nativeQuery = true)
    public List<SalesByClient> findSalesByClient(Double min);

}
