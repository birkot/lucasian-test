package com.lucasian.test_jlvl.repository;

import com.lucasian.test_jlvl.entity.Articles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.io.Serializable;
import java.util.List;

public interface ArticlesRepository  extends JpaRepository<Articles, Serializable> {

    @Query("FROM Articles")
    public List<Articles> getAll();

    @Query("FROM Articles WHERE code = ?1")
    public Articles getByCode(String code);

}
