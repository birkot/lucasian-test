package com.lucasian.test_jlvl.entity;

public interface SalesByClient {
    String getDocument();
    String getName();
    String getAddress();
    Double getTotal();
}
