package com.lucasian.test_jlvl.entity;

import java.math.BigDecimal;

public interface ArticleByMonth {
    String getMonth_sale();
    String getName();
    String getCode();
    Double getCant();
    BigDecimal getTotal();
}
