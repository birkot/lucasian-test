package com.lucasian.test_jlvl.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Partners implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String vat;

    @Column(nullable = false)
    private String nameCompany;

    @Column(nullable = false)
    private String address;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Partners() {
    }

    public Partners(String vat, String nameCompany, String address) {
        this.vat = vat;
        this.nameCompany = nameCompany;
        this.address = address;
    }
}
