package com.lucasian.test_jlvl.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
public class InvoiceDetails implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinTable(
            name="InvoiceDetailsPK",
            joinColumns = @JoinColumn(name = "details_id"),
            inverseJoinColumns = @JoinColumn(name="invoive_id")
    )
    @JsonIgnore
    private Invoice invoice;

    @ManyToOne
    @JoinColumn(name = "article_id", nullable=false)
    private Articles article;

    @Column
    private Double cant;

    @Column
    private BigDecimal price;

    @Column
    private BigDecimal subt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }


    public Articles getArticle() {
        return article;
    }

    public void setArticle(Articles article) {
        this.article = article;
    }

    public Double getCant() {
        return cant;
    }

    public void setCant(Double cant) {
        this.cant = cant;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getSubt() {
        return subt;
    }

    public void setSubt(BigDecimal subt) {
        this.subt = subt;
    }

    public InvoiceDetails() {
    }
}
