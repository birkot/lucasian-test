import {Component, Inject, OnInit} from '@angular/core';
import {DatasourceService} from '../datasource.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
export interface DialogData {
  vat: string;
  nameCompany: string;
  address: string;
}
@Component({
  selector: 'app-dialog-add-partners',
  templateUrl: './dialog-add-partners.component.html',
  styleUrls: ['./dialog-add-partners.component.css']
})
export class DialogAddPartnersComponent implements OnInit {

  constructor(private datasourceService: DatasourceService, public dialogRef: MatDialogRef<DialogAddPartnersComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit(): void {
  }

  closeDialog(): void{
    this.dialogRef.close();
  }

  doAction(): void {
    this.datasourceService.addPartners(this.data).subscribe(data => {
      console.log(data);
    });
    this.dialogRef.close();
  }
}
