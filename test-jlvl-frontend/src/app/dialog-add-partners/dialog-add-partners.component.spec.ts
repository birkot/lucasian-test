import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAddPartnersComponent } from './dialog-add-partners.component';

describe('DialogAddPartnersComponent', () => {
  let component: DialogAddPartnersComponent;
  let fixture: ComponentFixture<DialogAddPartnersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAddPartnersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAddPartnersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
