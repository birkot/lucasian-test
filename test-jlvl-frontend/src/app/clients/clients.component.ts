import {Component, OnInit, ViewChild} from '@angular/core';
import { DatasourceService } from 'src/app/datasource.service';
import {MatDialog} from '@angular/material/dialog';
import {DialogAddClientsComponent} from '../dialog-add-clients/dialog-add-clients.component';
import {Router} from '@angular/router';
@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {

  tableClients: any;
  dtOptions: any;
  tableData: any;
  @ViewChild('tableClients', {static: false}) table;

  constructor(private router: Router, private datasourceService: DatasourceService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getAllClients();
  }
  openDialogAddCliente(): void {
    const dialogRef = this.dialog.open(DialogAddClientsComponent, {
      width: '500px',
      data: {document: '', firstName: '', lastName: '', address: ''}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.tableClients = $(this.table.nativeElement);
      this.tableClients.DataTable().destroy();
      this.ngOnInit();
    });
  }

  // tslint:disable-next-line:typedef
  getAllClients() {
    this.datasourceService.getClients().subscribe(data => {
      console.log(data);
      this.tableData = data;
      this.dtOptions = {
        data: this.tableData,
        columns: [
          {title: 'ID', data: 'id'},
          {title: 'Docuemento', data: 'document'},
          {title: 'Nombres', data: 'firstName'},
          {title: 'Apellidos', data: 'lastName'},
          {title: 'Dirección', data: 'address'},
          {
            data: 'id',
            // tslint:disable-next-line:no-shadowed-variable typedef
            render(data, type, row) {
              return `<button mat-icon-button color="warn" data-element-id="${data}">
              <mat-icon>trash</mat-icon></button>`;
            }
          }
        ],
        // tslint:disable-next-line:no-shadowed-variable ban-types
        rowCallback: (row: Node, data: any[] | Object, index: number) => {
          const self = this;
          // Unbind first in order to avoid any duplicate handler
          // (see https://github.com/l-lin/angular-datatables/issues/87)
          $('td button', row).unbind('click');
          $('td button', row).bind('click', () => {
            // console.log(data);
            self.deleteClient(data);
          });
          return row;
        }
      };
      this.tableClients = $(this.table.nativeElement);
      this.tableClients.DataTable(this.dtOptions);
    }, err => {}, () => {});
  }

  deleteClient(data): void{
    this.datasourceService.deleteClients(data).subscribe(data2 => {
        this.tableClients = $(this.table.nativeElement);
        this.tableClients.DataTable().destroy();
        this.ngOnInit();
    });
  }

}
