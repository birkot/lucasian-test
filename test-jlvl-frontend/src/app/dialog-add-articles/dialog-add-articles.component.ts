import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DatasourceService} from '../datasource.service';
import {MatSnackBar} from '@angular/material/snack-bar';
export interface DialogData {
  code: string;
  name: string;
  partner: {
    id: number,
    vat: string,
    nameCompany: string
  };
}
@Component({
  selector: 'app-dialog-add-articles',
  templateUrl: './dialog-add-articles.component.html',
  styleUrls: ['./dialog-add-articles.component.css']
})
export class DialogAddArticlesComponent implements OnInit {

  constructor(private datasourceService: DatasourceService, public dialogRef: MatDialogRef<DialogAddArticlesComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  closeDialog(): void{
    this.dialogRef.close();
  }

  doAction(): void {
    console.log(this.data);
    this.datasourceService.addArticles(this.data).subscribe(data => {
      console.log(data);
    });
    this.dialogRef.close();
  }

  // tslint:disable-next-line:typedef
  searchPartner(){
    this.datasourceService.getPartnesByVat(this.data.partner.vat).subscribe(data => {
      if (data) {
        this.data.partner.id = data.id;
        this.data.partner.nameCompany = data.nameCompany;
      }else{
        this.snackBar.open('Proveedor no encontrado', 'ok', {duration: 2000});
      }
    });
  }

}
