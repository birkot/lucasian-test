import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAddArticlesComponent } from './dialog-add-articles.component';

describe('DialogAddArticlesComponent', () => {
  let component: DialogAddArticlesComponent;
  let fixture: ComponentFixture<DialogAddArticlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAddArticlesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAddArticlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
