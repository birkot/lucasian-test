import {Component, Inject, OnInit} from '@angular/core';
import {MatDialogModule, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {DatasourceService} from '../datasource.service';
export interface DialogData {
  document: string;
  firstName: string;
  lastName: string;
  address: string;
}

@Component({
  selector: 'app-dialog-add-clients',
  templateUrl: './dialog-add-clients.component.html',
  styleUrls: ['./dialog-add-clients.component.css']
})
export class DialogAddClientsComponent implements OnInit {

  constructor(private datasourceService: DatasourceService, public dialogRef: MatDialogRef<DialogAddClientsComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit(): void {
  }

  closeDialog(): void{
    this.dialogRef.close();
  }

  doAction(): void {
    console.log(this.data);
    this.datasourceService.addClients(this.data).subscribe(data => {
      console.log(data);
    });
    this.dialogRef.close();
  }

}
