export class Invoices {
  id: number;
  number: string;
  // tslint:disable-next-line:variable-name
  date_invoice: string;
  client: any;
  // tslint:disable-next-line:variable-name
  sub_total: number;
  tax: number;
  total: number;
  details: any;
}
