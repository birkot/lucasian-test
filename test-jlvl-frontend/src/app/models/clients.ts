export class Clients {
  id: number;
  document: string;
  firstName: string;
  lastName: string;
  address: string;
}
