export class Partners {
  id: number;
  vat: string;
  nameCompany: string;
  address: string;
}
