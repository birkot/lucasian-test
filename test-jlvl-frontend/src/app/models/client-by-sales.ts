export class ClientBySales {
  document: string;
  name: string;
  address: string;
  total: number;
}
