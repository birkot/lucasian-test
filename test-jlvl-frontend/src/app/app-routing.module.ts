import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import {ClientsComponent} from './clients/clients.component';
import {PartnersComponent} from './partners/partners.component';
import {ArticlesComponent} from './articles/articles.component';
import {InvoicesComponent} from './invoices/invoices.component';
import {AddInvoiceComponent} from './add-invoice/add-invoice.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent
  },
  {
  path: 'clients',
  component: ClientsComponent
  },
  {
    path: 'partners',
    component: PartnersComponent
  },
  {
    path: 'articles',
    component: ArticlesComponent
  },
  {
    path: 'invoices',
    component: InvoicesComponent
  },
  {
    path: 'addInvoice',
    component: AddInvoiceComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
