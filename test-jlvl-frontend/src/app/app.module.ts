import { BrowserModule } from '@angular/platform-browser';
import {Component, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import { RootNavComponent } from './root-nav/root-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HttpClientModule} from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { ClientsComponent } from './clients/clients.component';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import { DialogAddClientsComponent } from './dialog-add-clients/dialog-add-clients.component';
import {MatFormFieldControl, MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {FormsModule} from '@angular/forms';
import { PartnersComponent } from './partners/partners.component';
import { DialogAddPartnersComponent } from './dialog-add-partners/dialog-add-partners.component';
import { ArticlesComponent } from './articles/articles.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { registerLocaleData } from '@angular/common';
import localeCo from '@angular/common/locales/es-CO';
import { AddInvoiceComponent } from './add-invoice/add-invoice.component';
import {MatTableModule} from '@angular/material/table';
import { DialogAddArticleInvoiceComponent } from './dialog-add-article-invoice/dialog-add-article-invoice.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { DialogAddArticlesComponent } from './dialog-add-articles/dialog-add-articles.component';
registerLocaleData(localeCo, 'es-CO');

@NgModule({
  declarations: [
    AppComponent,
    RootNavComponent,
    DashboardComponent,
    ClientsComponent,
    DialogAddClientsComponent,
    PartnersComponent,
    DialogAddPartnersComponent,
    ArticlesComponent,
    InvoicesComponent,
    AddInvoiceComponent,
    DialogAddArticleInvoiceComponent,
    DialogAddArticlesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    LayoutModule,
    MatButtonModule,
    MatSidenavModule,
    MatListModule,
    MatCardModule,
    MatGridListModule,
    HttpClientModule,
    DataTablesModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    FormsModule,
    MatTableModule,
    MatSnackBarModule
  ],
  entryComponents: [
    DialogAddClientsComponent,
    DialogAddPartnersComponent,
    DialogAddArticleInvoiceComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
