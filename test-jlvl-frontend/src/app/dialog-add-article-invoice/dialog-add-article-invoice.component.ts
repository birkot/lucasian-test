import {Component, Inject, OnInit} from '@angular/core';
import {DatasourceService} from '../datasource.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
export interface DialogData {
  article: any;
  code: string;
  name: string;
  cant: number;
  price: number;
  subt: number;
}
@Component({
  selector: 'app-dialog-add-article-invoice',
  templateUrl: './dialog-add-article-invoice.component.html',
  styleUrls: ['./dialog-add-article-invoice.component.css']
})
export class DialogAddArticleInvoiceComponent{
  // tslint:disable-next-line:variable-name
  article_id: number;
  code: string;
  article: {
    id: number;
  };
  name: string;
  cant: number;
  price: number;
  subt: number;
  // tslint:disable-next-line:variable-name
  constructor(private datasourceService: DatasourceService, public dialogRef: MatDialogRef<DialogAddArticleInvoiceComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData, private snackBar: MatSnackBar) { }

  // tslint:disable-next-line:typedef
  searchArticle(){
    this.datasourceService.getArticlesByCode(this.code).subscribe(data => {
      if (data) {
        this.name = data.name;
        this.article_id = data.id;
      }else{
        this.snackBar.open('Artículo no encontrado', 'ok', {duration: 2000});
      }
    });
  }

  closeDialog(): void{
    this.dialogRef.close();
  }

  doAction(): void {
    if (this.article_id && this.cant && this.price){
      this.data.article.id = this.article_id;
      this.data.code = this.code;
      this.data.name = this.name;
      this.data.cant = this.cant;
      this.data.price = this.price;
      this.data.subt = this.subt;
    }
    this.dialogRef.close(this.data);
  }

  // tslint:disable-next-line:typedef
  onChange(){
    if (this.cant && this.price) {
      this.subt = this.cant * this.price;
    }
  }

  // tslint:disable-next-line:typedef
  changeCode(){
    // tslint:disable-next-line:triple-equals
    if (this.code ? (this.code == '') : true ){
      this.article_id = null;
      this.name = null;
    }else{
      this.searchArticle();
    }
  }

}
