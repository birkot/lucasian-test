import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAddArticleInvoiceComponent } from './dialog-add-article-invoice.component';

describe('DialogAddArticleInvoiceComponent', () => {
  let component: DialogAddArticleInvoiceComponent;
  let fixture: ComponentFixture<DialogAddArticleInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAddArticleInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAddArticleInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
