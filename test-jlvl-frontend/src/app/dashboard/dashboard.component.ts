import {Component, OnInit, ViewChild} from '@angular/core';
import {DatasourceService} from '../datasource.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  tableSales: any;
  dtOptions: any;
  tableData: any;
  @ViewChild('tableSales', {static: false}) table;

  tableNotSales: any;
  dtOptionsNotSales: any;
  tableDataNotSales: any;
  @ViewChild('tableNotSales', {static: false}) tableNot;

  tableSalesSalesClients: any;
  dtOptionsSalesClients: any;
  tableDataSalesClients: any;
  @ViewChild('tableSalesClients', {static: false}) tableSalesClients;

  constructor(private datasourceService: DatasourceService) { }



  ngOnInit(): void {
    this.getSalesByMonth();
    this.getClientBySales();
    this.getArticlesNotSales();
  }

  // tslint:disable-next-line:typedef
  getSalesByMonth() {
    this.datasourceService.getArticleByMonth().subscribe(data => {
      console.log(data);
      this.tableData = data;
      this.dtOptions = {
        data: this.tableData,
        columns: [
          {title: 'Código', data: 'code'},
          {title: 'Nombre', data: 'name'},
          {title: 'Cantidad', data: 'cant'},
          {title: 'Total', data: 'total'}
        ],
        rowGroup: {
          dataSrc: 'month_sale'
        }
      };
      this.tableSales = $(this.table.nativeElement);
      this.tableSales.DataTable(this.dtOptions);
    }, err => {}, () => {});
  }

  // tslint:disable-next-line:typedef
  getArticlesNotSales() {
    this.datasourceService.getArticleNotSales().subscribe(data => {
      console.log(data);
      this.tableDataNotSales = data;
      this.dtOptionsNotSales = {
        data: this.tableDataNotSales,
        columns: [
          {title: 'Código', data: 'code'},
          {title: 'Nombre', data: 'name'},
        ]
      };
      this.tableNotSales = $(this.tableNot.nativeElement);
      this.tableNotSales.DataTable(this.dtOptionsNotSales);
    }, err => {}, () => {});
  }

// tslint:disable-next-line:typedef
  getClientBySales() {
    this.datasourceService.getClientBySales().subscribe(data => {
      this.tableDataSalesClients = data;
      this.dtOptionsSalesClients = {
        data: this.tableDataSalesClients,
        columns: [
          {title: 'Documento', data: 'document'},
          {title: 'Nombre', data: 'name'},
          {title: 'Dirección', data: 'address'},
          {title: 'Total', data: 'total'}
        ]
      };
      this.tableSalesClients = $(this.tableSalesClients.nativeElement);
      this.tableSalesClients.DataTable(this.dtOptionsSalesClients);
    }, err => {}, () => {});
  }

}
