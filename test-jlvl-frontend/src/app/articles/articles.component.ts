import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {DatasourceService} from '../datasource.service';
import {MatDialog} from '@angular/material/dialog';
import {DialogAddClientsComponent} from '../dialog-add-clients/dialog-add-clients.component';
import {DialogAddArticlesComponent} from '../dialog-add-articles/dialog-add-articles.component';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {
  tableArticles: any;
  dtOptions: any;
  tableData: any;
  @ViewChild('tableArticles', {static: false}) table;

  constructor(private router: Router, private datasourceService: DatasourceService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getAllArticles();
  }

  openDialogAddArticles(): void {
    const dialogRef = this.dialog.open(DialogAddArticlesComponent, {
      width: '500px',
      data: {code: '', name: '', partner: {nameCompany: '', vat: ''}}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.tableArticles = $(this.table.nativeElement);
      this.tableArticles.DataTable().destroy();
      this.ngOnInit();
    });
  }

  // tslint:disable-next-line:typedef
  getAllArticles() {
    this.datasourceService.getArticles().subscribe(data => {
      console.log(data);
      this.tableData = data;
      this.dtOptions = {
        data: this.tableData,
        columns: [
          {title: 'ID', data: 'id'},
          {title: 'Código', data: 'code'},
          {title: 'Descripción', data: 'name'},
          {title: 'Proveedor', data: 'partner.nameCompany'},
          {
            data: 'id',
            // tslint:disable-next-line:no-shadowed-variable typedef
            render(data, type, row) {
              return `<button mat-icon-button color="warn" data-element-id="${data}">
              <mat-icon>trash</mat-icon></button>`;
            }
          }
        ],
        // tslint:disable-next-line:no-shadowed-variable ban-types
        rowCallback: (row: Node, data: any[] | Object, index: number) => {
          const self = this;
          $('td button', row).unbind('click');
          $('td button', row).bind('click', () => {
            // console.log(data);
            self.deleteArticles(data);
          });
          return row;
        }
      };
      this.tableArticles = $(this.table.nativeElement);
      this.tableArticles.DataTable(this.dtOptions);
    }, err => {}, () => {});
  }
  deleteArticles(data): void{
    this.datasourceService.deleteArticles(data).subscribe(data2 => {
      this.tableArticles = $(this.table.nativeElement);
      this.tableArticles.DataTable().destroy();
      this.ngOnInit();
    });
  }

}
