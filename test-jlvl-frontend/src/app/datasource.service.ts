import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Clients} from './models/clients';
import {catchError, retry} from 'rxjs/operators';
import {Partners} from './models/partners';
import {Articles} from './models/articles';
import {Invoices} from './models/invoices';
import {ArticleByMonth} from './models/article-by-month';
import {ClientBySales} from './models/client-by-sales';

@Injectable({
  providedIn: 'root'
})
export class DatasourceService {
  apiPath = 'http://localhost:8080/api/';

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // tslint:disable-next-line:typedef
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  // Obtener la lista de clientes
  getClients(): Observable<Clients> {
    return this.http
      .get<Clients>(this.apiPath + 'clients')
      .pipe(
        catchError(this.handleError)
      );
  }
  // Obtener la lista de clientes
  getClientsByDocument(data): Observable<Clients> {
    const httpParams = new HttpParams().set('document', data);

    const options = { params: httpParams };
    return this.http
      .get<Clients>(this.apiPath + 'clients/search', options)
      .pipe(
        catchError(this.handleError)
      );
  }

  // agregar cliente
  addClients(data): Observable<Clients> {
    return this.http
      .post<Clients>(this.apiPath + 'clients', data)
      .pipe(
        catchError(this.handleError)
      );
  }
  // eliminar cliente
  deleteClients(id): Observable<ArrayBuffer> {
    console.log(id);
    const httpParams = new HttpParams().set('id', id.id);

    const options = { params: httpParams };
    // @ts-ignore
    return this.http.delete( this.apiPath + 'clients', options)
      .pipe(
        catchError(this.handleError)
      );
  }

  // Obtener la lista de proveedores
  getPartners(): Observable<Partners> {
    return this.http
      .get<Partners>(this.apiPath + 'partners')
      .pipe(
        catchError(this.handleError)
      );
  }

  // agregar proveedor
  addPartners(data): Observable<Partners> {
    return this.http
      .post<Partners>(this.apiPath + 'partners', data)
      .pipe(
        catchError(this.handleError)
      );
  }

  // eliminar cliente
  deletePartners(id): Observable<ArrayBuffer> {
    const httpParams = new HttpParams().set('id', id.id);
    const options = { params: httpParams };
    // @ts-ignore
    return this.http.delete( this.apiPath + 'partners', options)
      .pipe(
        catchError(this.handleError)
      );
  }

  // Obtener un proveedor por vat
  getPartnesByVat(data): Observable<Partners> {
    const httpParams = new HttpParams().set('vat', data);
    const options = { params: httpParams };
    return this.http
      .get<Partners>(this.apiPath + 'partners/search', options)
      .pipe(
        catchError(this.handleError)
      );
  }

  // Obtener la lista de articulos
  getArticles(): Observable<Articles> {
    return this.http
      .get<Articles>(this.apiPath + 'articles')
      .pipe(
        catchError(this.handleError)
      );
  }

  // agregar artículo
  addArticles(data): Observable<Articles> {
    return this.http
      .post<Articles>(this.apiPath + 'articles', data)
      .pipe(
        catchError(this.handleError)
      );
  }

  // Obtener un articulo por código
  getArticlesByCode(data): Observable<Articles> {
    const httpParams = new HttpParams().set('code', data);
    const options = { params: httpParams };
    return this.http
      .get<Articles>(this.apiPath + 'articles/search', options)
      .pipe(
        catchError(this.handleError)
      );
  }

  // eliminar articulo
  deleteArticles(id): Observable<ArrayBuffer> {
    const httpParams = new HttpParams().set('id', id.id);
    const options = { params: httpParams };
    // @ts-ignore
    return this.http.delete( this.apiPath + 'articles', options)
      .pipe(
        catchError(this.handleError)
      );
  }

  // Obtener la lista de facturas
  getInvoices(): Observable<Invoices> {
    return this.http
      .get<Invoices>(this.apiPath + 'invoices')
      .pipe(
        catchError(this.handleError)
      );
  }



  // agregar factura
  addInvoice(data): Observable<Invoices> {
    return this.http
      .post<Invoices>(this.apiPath + 'invoices', data)
      .pipe(
        catchError(this.handleError)
      );
  }

  // Obtener articulos vendidos por mes
  getArticleByMonth(): Observable<ArticleByMonth> {
    return this.http
      .get<ArticleByMonth>(this.apiPath + 'articles/sales/bymonth')
      .pipe(
        catchError(this.handleError)
      );
  }

  // Obtener articulos vendidos por mes
  getArticleNotSales(): Observable<ArticleByMonth> {
    return this.http
      .get<ArticleByMonth>(this.apiPath + 'articles/notsales')
      .pipe(
        catchError(this.handleError)
      );
  }

  // Obtener articulos vendidos por mes
  getClientBySales(): Observable<ClientBySales> {
    const httpParams = new HttpParams().set('min', '150000');
    const options = { params: httpParams };
    // @ts-ignore
    return this.http
      .get<ClientBySales>(this.apiPath + 'clients/sales', options)
      .pipe(
        catchError(this.handleError)
      );
  }

}
