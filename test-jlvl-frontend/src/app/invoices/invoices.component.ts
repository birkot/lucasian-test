import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {DatasourceService} from '../datasource.service';
import {MatDialog} from '@angular/material/dialog';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit {
  tableInvoices: any;
  dtOptions: any;
  tableData: any;
  @ViewChild('tableInvoices', {static: false}) table;

  constructor(private router: Router, private datasourceService: DatasourceService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getAllInvoices();
  }

  // tslint:disable-next-line:typedef
  getAllInvoices() {
    this.datasourceService.getInvoices().subscribe(data => {
      console.log(data);
      this.tableData = data;
      this.dtOptions = {
        data: this.tableData,
        columns: [
          {title: 'ID', data: 'id'},
          {title: 'Número', data: 'number'},
          {title: 'Fecha Factura', data: 'date_invoice',
            // tslint:disable-next-line:no-shadowed-variable
            render(data, type, row): string {
              return `${formatDate(data, 'medium', 'es-CO')}`;
            }
          },
          {title: 'Cliente', data: 'client',

            // tslint:disable-next-line:no-shadowed-variable
            render(data, type, row): string {
              return `${data.lastName} ${data.firstName}`;
            }
          },
          {
            data: 'id',
            // tslint:disable-next-line:no-shadowed-variable typedef
            render(data, type, row) {
              return `<button mat-icon-button color="warn" data-element-id="${data}">
              <mat-icon>trash</mat-icon></button>`;
            }
          },

        ],
        // tslint:disable-next-line:no-shadowed-variable ban-types
        rowCallback: (row: Node, data: any[] | Object, index: number) => {
          const self = this;
          // Unbind first in order to avoid any duplicate handler
          // (see https://github.com/l-lin/angular-datatables/issues/87)
          $('td button', row).unbind('click');
          $('td button', row).bind('click', () => {
            // console.log(data);
            // self.deleteArticles(data);
          });
          return row;
        }
      };
      this.tableInvoices = $(this.table.nativeElement);
      this.tableInvoices.DataTable(this.dtOptions);
    }, err => {}, () => {});
  }

}
