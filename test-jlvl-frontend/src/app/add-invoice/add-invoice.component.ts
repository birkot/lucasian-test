import {Component, OnInit, ViewChild} from '@angular/core';
import {DatasourceService} from '../datasource.service';
import {MatDialog} from '@angular/material/dialog';
import {DialogAddArticleInvoiceComponent, DialogData} from '../dialog-add-article-invoice/dialog-add-article-invoice.component';
import Table = WebAssembly.Table;
import {MatTable} from '@angular/material/table';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Invoices} from '../models/invoices';
import { Router} from '@angular/router';
export interface Transaction {
  article: {
    id: number;
  };
  code: string;
  name: string;
  cant: number;
  price: number;
  subt: number;
}
export class Clients {
  id: number;
  document: string;
  firstName: string;
  address: string;
}

@Component({
  selector: 'app-add-invoice',
  templateUrl: './add-invoice.component.html',
  styleUrls: ['./add-invoice.component.css']
})
export class AddInvoiceComponent implements OnInit {
  // @ts-ignore
  invoice: Invoices = {};
  idClient: number;
  document: string;
  name: string;
  address: string;
  number: string;
  date: string;
  displayedColumns = ['code', 'name', 'cant', 'price', 'subt'];
  transactions: Transaction[] = [
  ];
  @ViewChild('tblDetails') tblDetails: MatTable<any>;

  constructor(private datasourceService: DatasourceService, public dialog: MatDialog,
              private snackBar: MatSnackBar, private router: Router) { }

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  getTotalCost() {
    return this.transactions.map(t => t.subt).reduce((acc, value) => acc + value, 0);
  }

  addDetails(): void {
    const dialogRef = this.dialog.open(DialogAddArticleInvoiceComponent, {
      width: '650px',
      data: {article: {}}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.article.id) {
        // console.log(result.article_id);
        // agregar a la tabla
        this.transactions.push(result);
        this.tblDetails.renderRows();
      }
    });
  }

  // tslint:disable-next-line:typedef
  searchClient() {
    this.datasourceService.getClientsByDocument(this.document).subscribe(data => {
      if (data) {
        this.name = data.lastName + ' ' + data.firstName;
        this.address = data.address;
        this.idClient = data.id;
      }else {
        this.snackBar.open('Cliente no enconrado', 'ok', {duration: 2000});
      }
    });
  }

  // tslint:disable-next-line:typedef
  changeDocument() {
    // tslint:disable-next-line:triple-equals
    if (this.document ? (this.document == '') : true ){
      this.idClient = null;
      this.name = null;
      this.address = null;
    }else{
      this.searchClient();
    }
  }

  // tslint:disable-next-line:typedef
  addInvoice() {
    this.invoice.client = { id: this.idClient};
    this.invoice.number = this.number;
    this.invoice.date_invoice = this.date;
    this.invoice.total = this.getTotalCost();
    this.invoice.details = this.transactions;
    this.datasourceService.addInvoice(this.invoice).subscribe(data => {
      this.router.navigate(['/invoices']);
    });
  }
}
