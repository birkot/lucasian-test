import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {DatasourceService} from '../datasource.service';
import {MatDialog} from '@angular/material/dialog';
import {DialogAddClientsComponent} from '../dialog-add-clients/dialog-add-clients.component';
import {DialogAddPartnersComponent} from '../dialog-add-partners/dialog-add-partners.component';

@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.css']
})
export class PartnersComponent implements OnInit {

  tablePartnes: any;
  dtOptions: any;
  tableData: any;
  @ViewChild('tablePartners', {static: false}) table;

  constructor(private router: Router, private datasourceService: DatasourceService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getAllPartners();
  }

  // tslint:disable-next-line:typedef
  getAllPartners() {
    this.datasourceService.getPartners().subscribe(data => {
      console.log(data);
      this.tableData = data;
      this.dtOptions = {
        data: this.tableData,
        columns: [
          {title: 'ID', data: 'id'},
          {title: 'NIT', data: 'vat'},
          {title: 'Nombre de la Compañía', data: 'nameCompany'},
          {title: 'Dirección', data: 'address'},
          {
            data: 'id',
            // tslint:disable-next-line:no-shadowed-variable typedef
            render(data, type, row) {
              return `<button mat-icon-button color="warn" data-element-id="${data}">
              <mat-icon>trash</mat-icon></button>`;
            }
          }
        ],
        // tslint:disable-next-line:no-shadowed-variable ban-types
        rowCallback: (row: Node, data: any[] | Object, index: number) => {
          const self = this;
          // Unbind first in order to avoid any duplicate handler
          // (see https://github.com/l-lin/angular-datatables/issues/87)
          $('td button', row).unbind('click');
          $('td button', row).bind('click', () => {
            // console.log(data);
            self.deletePartner(data);
          });
          return row;
        }
      };
      this.tablePartnes = $(this.table.nativeElement);
      this.tablePartnes.DataTable(this.dtOptions);
    }, err => {}, () => {});
  }

  openDialogAddPartner(): void {
    const dialogRef = this.dialog.open(DialogAddPartnersComponent, {
      width: '500px',
      data: {vat: '', nameCompany: '', address: ''}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.tablePartnes = $(this.table.nativeElement);
      this.tablePartnes.DataTable().destroy();
      this.ngOnInit();
    });
  }

  deletePartner(data): void{
    this.datasourceService.deletePartners(data).subscribe(data2 => {
      this.tablePartnes = $(this.table.nativeElement);
      this.tablePartnes.DataTable().destroy();
      this.ngOnInit();
    });
  }

}
